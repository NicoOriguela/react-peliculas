import React from "react"

function Pelicula({ pelicula, peliculas, modificarPelicula ,modificarPeliculas }) {

    const elminarPelicula = id => {
        modificarPeliculas(peliculas.filter(pelicula => pelicula.id !== id));
    }

    return (
        <tr>    
            <td>{pelicula.nombre}</td>
            <td>{pelicula.fecha}</td>
            <td>
            <button
                    type="button"
                    className="btn btn-warning mr-2"
                    onClick={() => modificarPelicula(pelicula)}
                >Modificar</button>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => elminarPelicula(pelicula.id)}
                >Borrar</button>
            </td>
        </tr>
    )
}


export default Pelicula