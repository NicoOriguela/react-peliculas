import React, { useState, useEffect } from 'react'
import { v4 as uuidv4 } from 'uuid';
import Form from "./Form"
import Pelicula from "./Pelicula"

function Main() {

    let peliculasLS = JSON.parse(localStorage.getItem('peliculas')) || [] //<== es una forma de fijarse si estan vacio el LS

    const [peliculas, modificarPeliculas] = useState(peliculasLS);

    const [pelicula, modificarPelicula] = useState({
        id: '',
        nombre: '',
        fecha: ''
    })

    // const [peliculas, modificarPeliculas] = useState([JSON.parse(localStorage.getItem('peliculas')) || []]);  <== es otra forma de fijarse si estan vacio el LS

    const guardarPelicula = pelicula => {
        // let peliExiste = peliculas.filter(p => p.id === pelicula.id)

        if (pelicula.id !== '') {
            let index = peliculas.findIndex(p => p.id === pelicula.id)
            peliculas[index] = pelicula
            /*modificarPeliculas([
                ...peliculas.filter(p => p.id !== pelicula.id), pelicula
            ]);*/
        } else {
            pelicula.id = uuidv4()
            modificarPeliculas([
                ...peliculas, pelicula
            ]);
        }

    }

    useEffect(() => {
        //Guardar Peliculas en el localStorage
        localStorage.setItem('peliculas', JSON.stringify(peliculas))

    }, [peliculas])


    return (
        <div className="container mt-4">
            <header className="row">
                <h1 className="mx-auto py-3">PRÓXIMOS ESTRENOS</h1>
            </header>
            <div className="row">
                <div className="col-4">
                    <Form
                        pelicula={pelicula}
                        modificarPelicula={modificarPelicula}
                        guardarPelicula={guardarPelicula}
                    />
                </div>
                <div className="col-8">
                    {peliculas.length === 0 ?

                        <h3> Sin Pelicula</h3>

                        :

                        <table className="table text-left">
                            <thead className="thead-dark">
                                <tr>
                                    <th scope="col">Pelicula</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Borrar</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    peliculas.map(pelicula => (
                                        <Pelicula
                                            key={pelicula.id}
                                            pelicula={pelicula}
                                            peliculas={peliculas}
                                            modificarPelicula={modificarPelicula}
                                            modificarPeliculas={modificarPeliculas}
                                        />
                                    ))
                                }
                            </tbody>
                        </table>

                    }

                </div>
            </div>
        </div>
    )
}


export default Main