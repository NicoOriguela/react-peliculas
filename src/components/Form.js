import React, { useState } from 'react';
import PropTypes from 'prop-types';
// import { v4 as uuidv4 } from 'uuid';

function Form({ pelicula, modificarPelicula, guardarPelicula }) {

    

    const [error, modificarError] = useState(false);

    const { nombre, fecha } = pelicula

    const changeState = (e) => {
        console.log(e.target.name, e.target.value)
        modificarPelicula({
            ...pelicula,
            [e.target.name]: e.target.value
        })
    }

    const submitPelicula = (e) => {
        e.preventDefault()

        //Controles de campos
        if (pelicula.nombre === '' || pelicula.fecha === '') {
            // alert('Todos los campos son requeridos perreque')
            modificarError(true)
            return;
        }
        modificarError(false)

        

        //Guardar Peliculas
        guardarPelicula(pelicula);


        //Restaurar Formulario
        modificarPelicula({
            id: '',
            nombre: '',
            fecha: ''
        })


    }


    return (
        <form className="text-left" onSubmit={submitPelicula}>
            <h2 className="text-left">Agregar pelicula</h2>

            {error ?
                <div className="alert alert-warning" role="alert">
                    Complete todos los campos perreque
                </div> : null
            }

            <div className="form-grup">
                <div>
                    <label htmlFor="nombre">Pelicula</label>
                </div>
                <input
                    className="my-1"
                    type="text"
                    name="nombre"
                    id="nombre"
                    placeholder="Nombre"
                    onChange={changeState}
                    value={nombre}
                />
            </div>
            <div className="form-grup">
                <div>
                    <label htmlFor="fecha">Fecha de estreno</label>
                </div>
                <input
                    className="my-1 font-weight-light"
                    type="date"
                    name="fecha"
                    id="fecha"
                    onChange={changeState}
                    value={fecha}
                />
            </div>
            <button className="btn btn-primary my-1" type="submit">Enviar</button>
        </form>
    )
}

Form.propTypes = {
    pelicula: PropTypes.object.isRequired,
    modificarPelicula : PropTypes.func.isRequired,
    guardarPelicula : PropTypes.func.isRequired
}

export default Form